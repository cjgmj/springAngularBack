package com.cjgmj.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.cjgmj.apirest.models.entity.Cliente;

public interface IClienteDao extends CrudRepository<Cliente, Long> {

}
